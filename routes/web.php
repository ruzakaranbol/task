<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('login', "Auth\LoginController@login")->name('login');
Route::get('logout', "Auth\LoginController@logout")->name('logout');
Route::get('signin', "Auth\LoginController@signin")->name('signin');

Route::group([
    'prefix' => 'admin',
    'middleware' => 'admin',
    ], function () {
        Route::get('create-user','UsersController@create')->name('create-user');
        Route::post('store-user','UsersController@store')->name('store-user');
        Route::get('list-users','UsersController@index')->name('list-users');
        Route::get('preview-user/{user}','UsersController@preview')->name('preview-user');


        Route::get('create-rank','RanksController@create')->name('create-rank');
        Route::post('store-rank','RanksController@store')->name('store-rank');
        Route::get('list-ranks','RanksController@index')->name('list-rank');

        Route::get('create-ship','ShipsController@create')->name('create-ship');
        Route::post('store-ship','ShipsController@store')->name('store-ship');
        Route::get('list-ship','ShipsController@index')->name('list-ships');
        Route::get('add-crew','ShipsController@addCrew')->name('add-crew');
        Route::post('store-crew-ship','ShipsController@storeCrewToShip')->name('store-crew-ship');

        Route::get('create-notification', 'NotificationsController@create')->name('create-notification');
        Route::post('send-notification', 'NotificationsController@send')->name('send-notification');
});

Route::group([
    'prefix' => "crew",
    'middleware' => 'crew'
    ], function () {
    Route::get('home', 'CrewController@home')->name('crew-home');
    Route::get('get-notification', 'CrewController@getNotification')->name('crew-notification');
    Route::get('mark-as-read/{notification}', 'CrewController@markAsRead')->name('mark-as-read');
});

Route::get('/home', 'IndexController@home')->name('home-page');
