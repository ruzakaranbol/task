
## project setup
Machine setup:
    php 7.3
    mariadb 10.4
    apache (also work with laravel builtin server- php artisan serve)

create empty database and put name into .env

commands to run in the root of the project:
    composer install
    php artisan migrate
    php artisan db:seed
    
you can login as admin with credential:
username: admin@admin.com
password: Admin@2020

**when you create new user it will be created with type crew and hardcoded password: XXX
