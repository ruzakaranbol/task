<?php

use Illuminate\Database\Seeder;

/**
 * Class UsersTableSeeder
 */
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('users')->insert([
            'name' => "Admin",
            'surname' => "Super",
            'email' => "admin@admin.com",
            'password' => bcrypt('Admin@2020'),
            'type' => \App\User::USER_TYPE_ADMIN,
            'created_by' => 1,
        ]);
    }
}
