<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Rank
 * @package App
 */
class Rank extends Model
{
    /**
     * @return HasMany
     */
    public function users()
    {
        return $this->hasMany('App\User', 'rank_id');
    }

    /**
     * @return mixed
     */
    public function getUsersWithRank()
    {
        return $this->where('is_deleted', 0)->with('rank')->get();
    }
}
