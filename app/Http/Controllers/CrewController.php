<?php

namespace App\Http\Controllers;

use App\Http\Middleware\CrewUser;
use App\Notification;
use App\Services\NotificationService;
use Illuminate\Http\Request;

/**
 * Class CrewController
 * @package App\Http\Controllers
 */
class CrewController extends Controller
{

    private $notificationService;

    /**
     * CrewController constructor.
     * @param NotificationService $notificationService
     */
    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function home()
    {
        return view('crew.home');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getNotification()
    {

        $notifications = $this->notificationService->prepareData(auth()->user());

        return view('crew.notifications', ['notifications' => $notifications]);
    }

    /**
     * @param Notification $notification
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function markAsRead(Notification $notification)
    {
        if ($notification->notifiable_id !== auth()->user()->id) {
            return back();
        }
        $notification->read_at = new \DateTime();
        $notification->save();

        return redirect()->route('crew-notification');
    }
}
