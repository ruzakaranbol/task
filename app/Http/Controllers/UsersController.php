<?php

namespace App\Http\Controllers;

use App\Rank;
use App\Services\UserService;
use App\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class UsersController
 * @package App\Http\Controllers
 */
class UsersController extends Controller
{

    private $userService;

    /**
     * UsersController constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        $users = User::with(['rank', 'ship'])->where('is_deleted', 0)
            ->where('type', User::USER_TYPE_CREW)
            ->get();

        return view('admin.users.index', ['users' => $users]);
    }

    /**
     * @return Factory|View
     */
    public function create()
    {
        $ranks = Rank::where('is_deleted', 0)->get();

        return view('admin.users.create', ['ranks' => $ranks]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:2|max:100',
            'surname' => 'required|min:2|max:100',
            'email' => 'required|email|unique:users',
            'selected-rank' => 'required|in:'.$this->userService->availableRanks(),
        ]);

        $this->userService->storeData($request);

        return redirect()->action('UsersController@index');
    }

    /**
     * @param User $user
     * @return Factory|View
     */
    public function preview(User $user)
    {
        return view('admin.users.preview', ['user' => $user]);
    }
}
