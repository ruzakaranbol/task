<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;

/**
 * Class LoginController
 * App\Http\Controllers\Auth
 */
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function login()
    {
        return view('login');
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse|Redirector
     */
    public function signin(Request $request)
    {

        $request->validate([
            'email' => 'required|email',
            'password' => "required",
        ]);

        if (Auth::attempt([
            'email' => $request->request->get('email'),
            'password' => $request->request->get('password'),
        ])) {
            return redirect()->intended(route('create-user'));
        };

        return redirect('login')
            ->withErrors(['email' => 'Username or password does not match records!!!'])
            ->withInput(['email' => $request->request->get('email')]);
    }


    /**
     * @return RedirectResponse|Redirector
     */
    public function logout()
    {
        Auth::logout();
        session()->forget('loggedInUserId');
        session()->flush();
        if (!auth()->check()) {
            return redirect(route('login'));
        }
    }
}
