<?php

namespace App\Http\Controllers;

use App\Notifications\NewMessage;
use App\Services\NotificationService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Rank;
use Illuminate\View\View;

/**
 * Class NotificationsController
 * @package App\Http\Controllers
 */
class NotificationsController extends Controller
{
    private $notificationService;

    /**
     * NotificationsController constructor.
     * @param NotificationService $notificationService
     */
    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    /**
     * @return Factory|View
     */
    public function create()
    {
        $ranks = Rank::where('is_deleted', 0)->get();

        return view('admin.notifications.create', ['ranks' => $ranks]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function send(Request $request)
    {
        $request->validate([
            'selected-rank' => 'required',
            'text-notification' => 'required',
        ]);

        $message = $this->notificationService->storeData($request);


        $rank = Rank::with('users')->where('id', $request->request->get('selected-rank'))->first();
        foreach ($rank->users as $user) {
            $user->notify(new NewMessage($message));
        }

        return redirect()->route('create-notification');
    }
}
