<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\RedirectResponse;

/**
 * Class IndexController
 * @package App\Http\Controllers
 */
class IndexController extends Controller
{

    /**
     * @return RedirectResponse
     */
    public function home()
    {
        if (auth()->user()->type === User::USER_TYPE_ADMIN) {
            return redirect()->action("UsersController@create");
        }

        if (auth()->user()->type === User::USER_TYPE_CREW) {
            return redirect()->route('crew-home');
        }
    }
}
