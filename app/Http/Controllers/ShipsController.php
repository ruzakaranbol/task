<?php

namespace App\Http\Controllers;

use App\Services\ShipService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Ship;
use App\User;
use Illuminate\View\View;

/**
 * Class ShipsController
 * @package App\Http\Controllers
 */
class ShipsController extends Controller
{
    private $shipService;

    /**
     * ShipsController constructor.
     * @param ShipService $shipService
     */
    public function __construct(ShipService $shipService)
    {
        $this->shipService = $shipService;
    }

    /**
     * @return Factory|View
     */
    public function create()
    {
        return view('admin.ships.create');
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:3',
            'serial_number' => 'required|min:8|max:8',
            'image' => 'required|file|mimes:jpeg,bmp,png',
        ]);

        $this->shipService->storeData($request);

        return redirect()->action('ShipsController@index');
    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        $ships = Ship::with('users')->where('is_deleted', 0)
            ->get();

        return view('admin.ships.index', ['ships' => $ships]);
    }

    /**
     * @return Factory|View
     */
    public function addCrew()
    {
        $ships = Ship::where('is_deleted', 0)->get();
        $users = User::usersHaveNoShip();

        return view('admin.ships.add-crew', ['ships' => $ships, 'users' => $users]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function storeCrewToShip(Request $request)
    {
        $request->validate([
            'ship' => 'required|',
            'users' => 'required|array',
            'users.*' => 'in:'.$this->shipService->usersPossibleValues(),
        ]);

        $this->shipService->storeShipToCrew($request);


        return redirect()->action('ShipsController@index');
    }
}
