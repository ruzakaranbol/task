<?php

namespace App\Http\Controllers;

use App\Services\RankService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Rank;
use Illuminate\View\View;

/**
 * Class RanksController
 * @package App\Http\Controllers
 */
class RanksController extends Controller
{

    private $rankService;

    /**
     * RanksController constructor.
     * @param RankService $rankService
     */
    public function __construct(RankService $rankService)
    {
        $this->rankService = $rankService;
    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        $ranks = Rank::where('is_deleted', 0)
            ->get();

        return view('admin.ranks.index', ['ranks' => $ranks]);
    }
    /**
     * @return Factory|View
     */
    public function create()
    {
        return view('admin.ranks.create');
    }

    /**
     * @return JsonResponse
     */
    public function get()
    {
        $ranks = Rank::all();

        return response()->json($ranks);
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:2|unique:ranks',
        ]);

        $this->rankService->storeData($request);

        return redirect()->action('RanksController@index');
    }
}
