<?php


namespace App\Services;


use App\Ship;
use App\Traits\PossibleValuesTrait;
use App\User;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;

class ShipService
{

    use PossibleValuesTrait;

    /**
     * @param Request $request
     * @return Ship
     */
    public function storeData(Request $request): Ship
    {
        $ship = new Ship();
        $ship->name = $request->request->get('name');
        $ship->serial_number = $request->request->get('serial_number');
        $ship->created_by = auth()->user()->id;
        $ship->image = $this->storeImage($request);

        $ship->save();

        return $ship;
    }

    /**
     * @return string
     */
    public function usersPossibleValues()
    {
        $users = User::usersHaveNoShip();

        return $this->getPossibleValues($users);
    }

    /**
     * @param Request $request
     */
    public function storeShipToCrew(Request $request)
    {
        foreach ($request->request->get('users') as $userId) {
            $user = User::find($userId);
            $user->ship_id = $request->request->get('ship');
            $user->save();
        }

        return;
    }

    /**
     * @param Request $request
     * @return string
     */
    private function storeImage(Request $request)
    {
        $directory = config('filesystems.images-uploads-path');
        $fileName = Str::slug(request()->input('name'), '-').".".$request->file('image')->getClientOriginalExtension();
        $request->file('image')->move(public_path($directory), $fileName);
        $image = $directory.$fileName;

        $img = Image::make(public_path($directory).$fileName);
        $img->resize(817, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        return $image;
    }



}
