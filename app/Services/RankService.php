<?php


namespace App\Services;


use App\Rank;
use Illuminate\Http\Request;

class RankService
{
    /**
     * @param Request $request
     * @return Rank
     */
    public function storeData(Request $request): Rank
    {
        $rank = new Rank();
        $rank->name = $request->request->get('name');
        $rank->created_by = auth()->user()->id;
        $rank->save();

        return $rank;
    }

}
