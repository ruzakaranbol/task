<?php


namespace App\Services;


use App\NotificationMessage;
use App\Rank;
use App\User;
use Illuminate\Http\Request;

class NotificationService
{
    /**
     * @param Request $request
     * @return NotificationMessage
     */
    public function storeData(Request $request): NotificationMessage
    {
        $message = new NotificationMessage();
        $message->rank_id = $request->request->get('selected-rank');
        $message->created_by = auth()->user()->id;
        $message->content = $this->stripNotAllowedTag($request->request->get('text-notification'));
        $message->save();

        return $message;
    }


    /**
     * @param User $user
     * @return array
     */
    public function prepareData(User $user)
    {
        $preparedNotifications = [];
        $this->formatNotifications($user->unreadNotifications, $preparedNotifications);
        $this->formatNotifications($user->readNotifications, $preparedNotifications);
        $this->sortNotification($preparedNotifications);

        return $preparedNotifications;
    }

    /**
     * @param $notifications
     * @param $preparedNotifications
     */
    private function formatNotifications($notifications, &$preparedNotifications)
    {
        foreach ($notifications as $notification) {
            $preparedNotifications[] = [
                'notification' => $notification->data['message'],
                'created_at' => $notification->created_at,
                'time' => strtotime($notification->created_at),
                'notification_id' => $notification->id,
                'read_at' => $notification->read_at,
            ];
        }
    }

    /**
     * @param array $preparedNotification
     */
    private function sortNotification(array &$preparedNotification)
    {
        usort($preparedNotification, function($a, $b) {
            return $b['time'] <=> $a['time'];
        });
    }

    /**
     * @param string $text
     * @return string
     */
    private function stripNotAllowedTag(string $text): string
    {
        return strip_tags($text,'<p><strong><em><bold><a>');
    }
}
