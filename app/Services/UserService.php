<?php


namespace App\Services;


use App\Rank;
use App\Traits\PossibleValuesTrait;
use Illuminate\Http\Request;
use App\User;

class UserService
{
    use PossibleValuesTrait;

    /**
     * @return string
     */
    public function availableRanks(): string
    {
        $ranks = Rank::where('is_deleted', 0)->get();

        return $this->getPossibleValues($ranks);
    }

    /**
     * @param Request $request
     * @return User
     */
    public function storeData(Request $request)
    {
        $user = new User();
        $user->name = $request->request->get('name');
        $user->surname = $request->request->get('surname');
        $user->email = $request->request->get('email');
        $user->rank_id = $request->request->get('selected-rank');
        $user->password = bcrypt('XXX');
        $user->created_by = auth()->user()->id;
        $user->save();

        return $user;
    }

}
