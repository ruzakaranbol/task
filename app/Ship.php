<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Ship
 * @package App
 */
class Ship extends Model
{

    /**
     * @return HasMany
     */
    public function users()
    {
        return $this->hasMany('App\User', 'ship_id');
    }
}
