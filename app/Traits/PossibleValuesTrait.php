<?php


namespace App\Traits;


/**
 * Trait PossibleValuesTrait
 * @package App\Traits
 */
trait PossibleValuesTrait
{
    /**
     * @param array $data
     * @return string
     */
    public function getPossibleValues(array $data): string
    {
        $availableValues = "";
        $count = count($data);
        foreach ($data as $key => $value) {
            $availableValues .= $value->id;
            if ($key+1 !== $count) {
                $availableValues .= ",";
            }
        }

        return $availableValues;
    }
}
