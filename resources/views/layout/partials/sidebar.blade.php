<!-- **********************************************************************************************************************************************************
    MAIN SIDEBAR MENU
    *********************************************************************************************************************************************************** -->
<!--sidebar start-->
<aside>
    <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
            @if(auth()->user()->type === \App\User::USER_TYPE_ADMIN)
            <li class="sub-menu">
                <a class="@if(
                            Route::currentRouteName() == 'create-user' ||
                            Route::currentRouteName() == 'list-users' ||
                            Route::currentRouteName() == 'preview-user'
                        ) active @endif" href="javascript:;">
                    <i class="fa fa-user"></i>
                    <span>Users</span>
                </a>
                <ul class="sub">
                    <li @if(Route::currentRouteName() == 'list-users') class="active" @endif><a href="{{ route('list-users') }}">List</a></li>
                    <li @if(Route::currentRouteName() == 'create-user') class="active" @endif><a href="{{ route('create-user') }}">Create</a></li>
                </ul>
            </li>
            <li class="sub-menu">
                <a class="@if(
                            Route::currentRouteName() == 'create-rank' ||
                            Route::currentRouteName() == 'list-rank'
                        ) active @endif" href="javascript:;">
                    <i class="fa fa-book"></i>
                    <span>Ranks</span>
                </a>
                <ul class="sub">
                    <li class="@if(Route::currentRouteName() == 'list-rank') active @endif"><a href="{{ route('list-rank') }}">List</a></li>
                    <li class="@if(Route::currentRouteName() == 'create-rank') active @endif"><a href="{{ route('create-rank') }}">Create</a></li>
                </ul>
            </li>
            <li class="sub-menu">
                <a class="@if(
                            Route::currentRouteName() == 'create-ship' ||
                            Route::currentRouteName() == 'list-ship' ||
                            Route::currentRouteName() == 'add-crew'
                        ) active @endif" href="javascript:;">
                    <i class="fa fa-tasks"></i>
                    <span>Ships</span>
                </a>
                <ul class="sub">
                    <li @if(Route::currentRouteName() == 'list-ships')class="active" @endif ><a href="{{ route('list-ships') }}">List</a></li>
                    <li @if(Route::currentRouteName() == 'create-ship')class="active"@endif ><a href="{{ route('create-ship') }}">Create</a></li>
                    <li @if(Route::currentRouteName() == 'add-crew')class="active"@endif ><a href="{{ route('add-crew') }}">Add crew</a></li>
{{--                    <li><a href="form_validation.html">Form Validation</a></li>--}}
{{--                    <li><a href="contactform.html">Contact Form</a></li>--}}
                </ul>
            </li>
            <li class="sub-menu">
                <a href="javascript:;">
                    <i class="fa fa-th"></i>
                    <span>Notifications</span>
                </a>
                <ul class="sub">
                    <li><a href="{{ route('create-notification') }}">Create notification</a></li>
                </ul>
            </li>
            @endif
            @if(auth()->user()->type === \App\User::USER_TYPE_CREW)
            <li>
                <a class="@if(Route::currentRouteName() == 'crew-notification') active @endif" href="{{ route('crew-notification') }}">
                    <i class="fa fa-envelope"></i>
                    <span>Notifications </span>
                    <span class="label label-theme pull-right mail-info">
                        <?php $user = auth()->user() ?>
                        {{ count($user->unreadNotifications) }}
                    </span>
                </a>
            </li>
            @endif
        <!-- sidebar menu end-->
    </div>
</aside>
<!--sidebar end-->
