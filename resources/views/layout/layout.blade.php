<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <title>Dashio - Bootstrap Admin Template</title>

    <!-- Favicons -->
    <link href="{{ asset('img/favicon.png') }}" rel="icon">
    <link href="{{ asset('img/apple-touch-icon.png') }}" rel="apple-touch-icon">

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('js/dashio/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!--external css-->
    <link href="{{ asset('js/dashio/font-awesome/css/font-awesome.css') }}" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="{{ asset('css/dashio/style_.css') }}" rel="stylesheet">
    <link href="{{ asset('css/dashio/style-responsive.css') }}" rel="stylesheet">
    @yield('custom-css')

    <!-- =======================================================
      Template Name: Dashio
      Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
      Author: TemplateMag.com
      License: https://templatemag.com/license/
    ======================================================= -->
</head>
<body>
<section id="container">
    <div id="vue_app">
        <!-- **********************************************************************************************************************************************************
            TOP BAR CONTENT & NOTIFICATIONS
            *********************************************************************************************************************************************************** -->
    @include('layout.partials.header')
    <!-- **********************************************************************************************************************************************************
            MAIN SIDEBAR MENU
            *********************************************************************************************************************************************************** -->
        <!--sidebar start-->
    @include('layout.partials.sidebar')
    <!--sidebar end-->
        @yield('content')
        @include('layout.partials.footer')
    </div>
</section>



<!-- js placed at the end of the document so the pages load faster -->
<script type="text/javascript">
    window.csrf_token = "{{ csrf_token() }}"
</script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/dashio/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('js/dashio/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/dashio/jquery-ui-1.9.2.custom.min.js') }}"></script>
<script src="{{ asset('js/dashio/jquery.ui.touch-punch.min.js') }}"></script>
<script class="include" type="text/javascript" src="{{ asset('js/dashio/jquery.dcjqaccordion.2.7.js') }}"></script>
<script src="{{ asset('js/dashio/jquery.scrollTo.min.js') }}"></script>
<script src="{{ asset('js/dashio/jquery.nicescroll.js') }}" type="text/javascript"></script>
<!--common script for all pages-->
<script src="{{ asset('/js/dashio/common-scripts.js') }}"></script>
<!--script for this page-->
@yield('custom-js')

</body>

</html>
