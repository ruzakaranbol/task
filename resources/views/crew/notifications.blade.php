@extends('layout.layout')
@section('content')

    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            <h3><i class="fa fa-angle-right"></i> Notification</h3>
            <div class="row mt">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-4 profile-text">
                            <div class="right-divider">
                                    @foreach($notifications as $notification)
                                    <div class="alert alert-danger">
                                        <b>Content:</b>
                                        {!!  $notification['notification']  !!}
                                        <br>
                                        <br>
                                        @if($notification['read_at'] === null)<a id="add-sticky" class="btn btn-success  btn-sm" href="{{ route('mark-as-read', ['notification' => $notification['notification_id']]) }}">Mark as read</a>@endif
                                    </div>
                                    @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
@endsection
@section('custom-js')

@endsection
