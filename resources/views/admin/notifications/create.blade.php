@extends('layout.layout')
@section('content')

    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            <div class="row mt">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-4 profile-text">
                            <div class="right-divider">
                                <form method="POST" action="{{route('send-notification')}}">
                                    @csrf
                                    <div class="content-panel">
                                        <h4><i class="fa fa-angle-right"></i> Create notification</h4>
                                        <div class="form-panel">
                                            <div class="form-group">
                                                <label>Rank</label>
                                                <select class="form-control"  name="selected-rank">
                                                    @if(count($ranks) > 0)
                                                        @foreach($ranks as $rank)
                                                            <option value="{{$rank->id}}" @if(old('selected-rank') == $rank->id) selected @endif >{{$rank->name}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label class="">Enter text</label>
                                                <textarea name="text-notification"></textarea>
                                            </div>
                                            <button type="submit" class="btn btn-success" >Send</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
@endsection
@section('custom-js')
    <script src="//cdn.ckeditor.com/4.6.2/basic/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('text-notification');
    </script>
@endsection
