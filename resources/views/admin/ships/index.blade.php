@extends('layout.layout')
@section('content')

    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            {{--            <h3><i class="fa fa-angle-right"></i> Create user</h3>--}}
            <div class="row mt">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-8 profile-text">
                            <div class="right-divider">
                                <div class="content-panel">
                                    <h4><i class="fa fa-angle-right"></i> List ranks</h4>
                                    <table class="table table-striped table-advance table-hover">
                                        <thead>
                                        <tr>
                                            <th><i class="fa fa-bullhorn"></i> Rank name</th>
                                            <th class="hidden-phone"><i class="fa fa-question-circle"></i> Users</th>
                                            <th><i class="fa fa-email"></i> image</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($ships) > 0)
                                            @foreach($ships as $ship)
                                            <tr>
                                                <td>
                                                    <a href="basic_table.html#">{{ $ship->name }}</a>
                                                </td>
                                                <td class="hidden-phone">
                                                    @if(count($ship->users))
                                                        @foreach($ship->users as $user)
                                                            {{ $user->name }} |
                                                        @endforeach
                                                    @endif
                                                </td>
                                                <td><img width="50px" src="{{ $ship->image }}"></td>
                                            </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </section>
        <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->

@endsection
