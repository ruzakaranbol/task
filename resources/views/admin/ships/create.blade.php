@extends('layout.layout')
@section('content')

    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            {{--            <h3><i class="fa fa-angle-right"></i> Create user</h3>--}}
            <div class="row mt">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-4 profile-text">
                            <div class="right-divider">
                                <form method="POST" action="{{route('store-ship')}}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="content-panel">
                                        <h4><i class="fa fa-angle-right"></i> Create ship</h4>
                                        <div class="form-panel {{ $errors->has('name') ? ' has-error' : '' }}">
                                            <div class="form-group">
                                                <label class="sr-only">Name</label>
                                                <input class="form-control" placeholder="Enter name(at least 3 chars)" value="{{ old('name') }}" type="text" name="name">
                                                @if ($errors->has('name'))
                                                    <p class="text-danger">{{ $errors->first('name') }}</p>
                                                @endif
                                            </div>
                                            <div class="form-group {{ $errors->has('serial_number') ? ' has-error' : '' }}">
                                                <label class="sr-only">Serial</label>
                                                <input class="form-control" placeholder="Enter 8 characters serial" value="{{ old('serial_number') }}" type="text" name="serial_number" maxlength="8" minlength="8">
                                                @if ($errors->has('serial_number'))
                                                    <p class="text-danger">{{ $errors->first('serial_number') }}</p>
                                                @endif
                                            </div>
                                            <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
                                                <label class="control-label col-md-3">Image Upload</label>
                                                <input class=""  type="file" name="image">
                                                @if ($errors->has('image'))
                                                    <p class="text-danger">{{ $errors->first('image') }}</p>
                                                @endif

                                            </div>
                                            <button type="submit" class="btn btn-success" >Create</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->

@endsection
@section('custom-js')
    <script src="{{ asset('js/dashio/jquery-ui-1.9.2.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/dashio/bootstrap-fileupload/bootstrap-fileupload.js') }}"></script>
    <script src="{{ asset('js/dashio/advanced-form-components.js') }}"></script>
@endsection
