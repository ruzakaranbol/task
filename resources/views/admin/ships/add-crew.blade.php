@extends('layout.layout')
@section('custom-css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')

    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            {{--            <h3><i class="fa fa-angle-right"></i> Create user</h3>--}}
            <div class="row mt">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-4 profile-text">
                            <div class="right-divider">
                                <form method="POST" action="{{route('store-crew-ship')}}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="content-panel">
                                        <div class="form-panel">
                                            <div class="form-group">
                                                <label>Select ship</label>
                                                <select class="form-control" name="ship">
                                                    @if(count($ships) > 0)
                                                        @foreach($ships as $ship)
                                                            <option value="{{$ship->id}}" @if(old('ship->id') == $ship->id) selected @endif >{{$ship->name}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Choose users</label>
                                                <select  name="users[]" id="multi-select" multiple="multiple" class="form-control input-circle" placeholder='Select filter'>

                                                    @if(count($users)>0)
                                                        @foreach($users as $user)
{{--                                                            <optgroup label="{{ $type->type}}">--}}
{{--                                                                @foreach($filters as $filter)--}}
{{--                                                                    @if($filter->type == $type->type)--}}
                                                                        <option value="{{ $user->id }}" @if(!empty(old('users')) && in_array($user->id , old('users'))) selected="selected" @endif >{{ $user->name }}</option>
{{--                                                            @endif--}}
{{--                                                        @endforeach--}}
                                                        @endforeach
                                                    @endif


                                                </select>
                                            </div>
                                            <button type="submit" class="btn btn-success" >Add</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->

@endsection
@section('custom-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script type="text/javascript">
        $('#multi-select').select2();
    </script>
@endsection
