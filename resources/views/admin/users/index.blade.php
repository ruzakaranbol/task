@extends('layout.layout')
@section('content')

    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            {{--            <h3><i class="fa fa-angle-right"></i> Create user</h3>--}}
            <div class="row mt">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-8 profile-text">
                            <div class="right-divider">
                                <div class="content-panel">
                                    <h4><i class="fa fa-angle-right"></i> List user</h4>
                                    <table class="table table-striped table-advance table-hover">
                                        <!--                <hr>-->
                                        <thead>
                                        <tr>
                                            <th><i class="fa fa-bullhorn"></i> Name</th>
                                            <th class="hidden-phone"><i class="fa fa-question-circle"></i> Surename</th>
                                            <th><i class="fa fa-email"></i> email</th>
                                            <th><i class=" fa fa-rank"></i> rank</th>
                                            <th><i class=" fa fa-rank"></i> ship</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($users as $user)
                                        <tr>
                                            <td>
                                                <a href="{{ route('preview-user', ['user' => $user->id]) }}">{{ $user->name }}</a>
                                            </td>
                                            <td class="hidden-phone">{{ $user->surname }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td><span class="label label-info label-mini">{{ $user->rank['name'] }}</span></td>
                                            <td><span class="label label-info label-mini">{{ $user->ship['name'] }}</span></td>
                                            <td>
                                                <button class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>
                                                <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button>
                                                <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button>
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </section>
        <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->

@endsection
