@extends('layout.layout')
@section('content')

    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            {{--            <h3><i class="fa fa-angle-right"></i> Create user</h3>--}}
            <div class="row mt">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-4 profile-text">
                            <div class="right-divider">
                                <form method="POST" action="{{route('store-user')}}">
                                    @csrf
                                    <div class="content-panel">
                                        <h4><i class="fa fa-angle-right"></i> Create user</h4>
                                        <div class="form-panel">
                                            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                                <label class="sr-only">Name</label>
                                                <input class="form-control" placeholder="Enter name" value="{{ old('name') }}" type="text" name="name">
                                                @if ($errors->has('name'))
                                                    <p class="text-danger">{{ $errors->first('name') }}</p>
                                                @endif
                                            </div>
                                            <div class="form-group {{ $errors->has('surname') ? ' has-error' : '' }}">
                                                <label class="sr-only">surname</label>
                                                <input class="form-control" placeholder="Enter surname" type="text" value="{{ old('surname') }}" name="surname">
                                                @if ($errors->has('surname'))
                                                    <p class="text-danger">{{ $errors->first('surname') }}</p>
                                                @endif
                                            </div>
                                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label class="sr-only">Email</label>
                                                <input class="form-control" placeholder="Enter email" type="text" value="{{ old('email') }}" name="email">
                                                @if ($errors->has('email'))
                                                    <p class="text-danger">{{ $errors->first('email') }}</p>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label>Rank</label>
                                                <select class="form-control"  name="selected-rank">
                                                    @if(count($ranks) > 0)
                                                        @foreach($ranks as $rank)
                                                            <option value="{{$rank->id}}" @if(old('selected-rank') == $rank->id) selected @endif >{{$rank->name}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            <button type="submit" class="btn btn-success" >Create</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                </div>
            </div>
        </section>
        <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
@endsection
@section('custom-js')
@endsection
