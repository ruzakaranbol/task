@extends('layout.layout')
@section('content')

    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            {{--            <h3><i class="fa fa-angle-right"></i> Create user</h3>--}}
            <div class="row mt">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-4 profile-text">
                            <div class="right-divider">
                                <h5>General info</h5>
                                <table class="table table-striped table-advance table-hover">
                                    <tbody>
                                        <tr>
                                            <th>Name</th>
                                            <td>{{ $user->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Surname</th>
                                            <td>{{ $user->surname }}</td>

                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td>{{ $user->email }}</td>
                                        </tr>
                                        <tr>
                                            <th>Rank</th>
                                            <td>{{ $user->rank->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Ship</th>
                                            <td>{{ $user->ship->name }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-4 profile-text">
                            <div class="left-side">
                                <h5>Readed notifications</h5>
                                @if(count($user->readNotifications))
                                    @foreach($user->readNotifications as $notification)
                                        <div class="alert alert-success">
                                            {!!  $notification->data['message'] !!}
                                            <br>
                                            <span class="left-side text-danger small">readed at {{ $notification->read_at }}</span>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4 profile-text">
                            <div class="left-side">
                                <h5>Unreaded notification</h5>
                                @if(count($user->unreadNotifications))
                                    @foreach($user->unreadNotifications as $notification)
                                        <div class="alert alert-danger">
                                            {!!  $notification->data['message'] !!}
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
@endsection
@section('custom-js')
@endsection
