@extends('layout.layout')
@section('content')

    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            {{--            <h3><i class="fa fa-angle-right"></i> Create user</h3>--}}
            <div class="row mt">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-8 profile-text">
                            <div class="right-divider">
                                <div class="content-panel">
                                    <h4><i class="fa fa-angle-right"></i> List ranks</h4>
                                    <table class="table table-striped table-advance table-hover">
                                        <thead>
                                        <tr>
                                            <th><i class="fa fa-bullhorn"></i> Rank name</th>
                                            <th class="hidden-phone"><i class="fa fa-question-circle"></i> Users</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($ranks) > 0)
                                            @foreach($ranks as $rank)
                                            <tr>
                                                <td>
                                                    {{ $rank->name }}
                                                </td>
                                                <td class="hidden-phone">
                                                    @if(count($rank->users))
                                                        @foreach($rank->users as $user)
                                                            {{ $user->name }} |
                                                        @endforeach
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </section>
        <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->

@endsection
