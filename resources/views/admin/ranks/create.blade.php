@extends('layout.layout')
@section('content')

    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            <div class="row mt">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-4 profile-text">
                            <div class="right-divider">
                                <form method="POST" action="{{route('store-rank')}}">
                                    @csrf
                                    <div>
                                        <div class="content-panel">
                                            <h4><i class="fa fa-angle-right"></i> Create rank</h4>
                                            <div class="form-panel">
                                                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                                    <label class="sr-only">Name</label>
                                                    <input class="form-control" placeholder="Rank name" value="{{ old('name') }}" type="text" name="name">
                                                    @if ($errors->has('name'))
                                                        <p class="text-danger">{{ $errors->first('name') }}</p>
                                                    @endif
                                                </div>
                                                <button type="submit" class="btn btn-default">Create</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
        <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->

@endsection
